﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerThree {
    public class HealthComponent : Component {

        public int _maxHealth {
            get;
            private set;
        }

        public int _currentHealth {
            get;
            set;
        }

        public HealthComponent(int maxHealth, int initialHealth = 0) : base("health") {
            _maxHealth = maxHealth;
            _currentHealth = initialHealth;
        }
    }
}
