﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerThree {
    public class Component {
        public string _name {
            protected set;
            get;
        }

        private Guid _guid;

        public Component(string name) {
            _name = name;
            _guid = new Guid();
        }
    }
}
