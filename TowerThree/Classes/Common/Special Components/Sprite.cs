﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerThree {
    public class Sprite {
        public Texture2D _texture { get; set; }
        public int _rows { get; set; }
        public int _columns { get; set; }

        // Animation (micro)
        public int _sRow; // starting row
        public int _sCol; // starting column
        public int _cAnimFrameCount; // framecount of current animation
        public int _currentFrame; // nth frame in the entire sheet
        public int _totalFrames; // loop to the 0th frame when _currentFrame reaches this
        public int _timeSinceLastFrame = 0;
        public int _millisecondsPerFrame = 1000 / 12;

        // Animation (macro)
        public Dictionary<string, AnimData> _animList;
        public Dictionary<string, Texture2D> _textureList;
        public string _currentAnimation;
        public bool flipX; // is sprite flipped?
        public bool _animLocked; // is animation locked?

        // Sprite
        public int _spriteWidth { get; set; }
        public int _spriteHeight { get; set; }
        public int _spriteLowerBound {
            get => _spriteHeight > _spriteWidth ? _spriteWidth : _spriteHeight;
        }

        public Sprite (Texture2D texture, int rows, int cols) {
            _texture = texture;
            _rows = rows;
            _columns = cols;
            _cAnimFrameCount = 0;
            flipX = false;
            _animLocked = false;
            _spriteWidth = _texture.Width / _columns;
            _spriteHeight = _texture.Height / _rows;
        }

        // Switch to a new animation state as defined in InitAnimations()
        public void ChangeAnimState(string anim, bool animLocked) {
            if (_animLocked) {
                return;
            }
            _animLocked = animLocked;

            if (_currentAnimation == anim) {
                return;
            }

            _currentAnimation = anim;

            AnimData _cAnimData = _animList[_currentAnimation];

            _currentFrame = (_cAnimData.SROW * _columns) + _cAnimData.SCOL;
            _totalFrames = _cAnimData.FRAMECOUNT;
            _cAnimFrameCount = 0;
            _timeSinceLastFrame = 0;
        }

        // Swap to a new texture
        public void ChangeTextures(string tex) {
            _texture = _textureList[tex];
        }

        public void SpriteUpdate(GameTime gameTime) {
            _timeSinceLastFrame += gameTime.ElapsedGameTime.Milliseconds;
            if (_timeSinceLastFrame > _millisecondsPerFrame) {
                _currentFrame++;
                _cAnimFrameCount++;
                _timeSinceLastFrame = 0;

                AnimData _cAnimData = _animList[_currentAnimation];
                // reset animation
                if (_cAnimFrameCount == _totalFrames) {
                    // remove lock
                    if (_animLocked) {
                        _animLocked = false;
                    }
                    // switch to default animation if it is iset
                    if (_animList[_currentAnimation].DEFAULT_TNAME != null) {
                        ChangeAnimState(_animList[_currentAnimation].DEFAULT_TNAME, false);
                        _cAnimData = _animList[_currentAnimation];
                    }

                    _sRow = _cAnimData.SROW;
                    _sCol = _cAnimData.SCOL;

                    _cAnimFrameCount = 0;
                    _currentFrame = (_sRow * _columns) + _sCol;
                }
            }
        }
    }
}
