﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerThree {
    public class AnimData {
        public static string TYPE = "ANIMDATA";
        public string NAME { get; set; }
        // Transition to this animation after, optional
        public string DEFAULT_TNAME { get; set; }
        public bool ANIM_LOCKED { get; set; }
        // SROW and SCOL are 0-indexed
        // START ROW
        public int SROW { get; set; }
        // START COL
        public int SCOL { get; set; }
        public int FRAMECOUNT { get; set; }
        public int FRAMESPEED { get; set; }

        public AnimData (string name, string default_tname, bool anim_locked, int srow, int scol, int fcount, int fspeed) {
            NAME = name;
            DEFAULT_TNAME = default_tname;
            SROW = srow;
            SCOL = scol;
            FRAMECOUNT = fcount;
            FRAMESPEED = fspeed;
        }

        public AnimData (string name, int srow, int scol, int fcount, int fspeed) {
            DEFAULT_TNAME = null;
            ANIM_LOCKED = false;
            NAME = name;
            SROW = srow;
            SCOL = scol;
            FRAMECOUNT = fcount;
            FRAMESPEED = fspeed;
        }
    }
}
