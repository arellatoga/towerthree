﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerThree {
    public class Message {
        public static string TYPE = "MESSAGE";
        public string _eventType;
        public string _sender;
        public List<string> _strings { get; private set; }
        public List<int> _ints { get; private set; }
        public List<float> _floats { get; private set; }
        // do not use this
        public List<object> _objects { get; private set; }

        public Message(string eventType, string sender,
                        List<string> strings = null,
                        List<int> ints = null,
                        List<float> floats = null,
                        List<object> objects = null) {
            _strings = strings;
            _ints = ints;
            _floats = floats;
            _objects = objects;
        }
    }
}
