﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerThree {
    public class Publisher<Message> {
        protected Dictionary<string, EventChannel<Message>> _eventChannels;

        public Publisher() {
            _eventChannels = new Dictionary<string, EventChannel<Message>>();
        }

        public EventChannel<Message> GetEventChanneler(string name) {
            return _eventChannels[name];
        }

        public bool AddEventChannel(EventChannel<Message> eventPool) {
            try {
                _eventChannels.Add(eventPool._eventName, eventPool);
                return true;
            }
            catch {
                return false;
            }
        }

        public void Trigger(string _eventName, Message item) {
            _eventChannels[_eventName].SendMessage(item);
        }
    }
}
