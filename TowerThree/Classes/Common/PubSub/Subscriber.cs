﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerThree {
    public class Subscriber<Message> {
        public string _name { get; set; }
        public delegate void SubscriberAction(Message item);
        public SubscriberAction _subscriberAction { get; set; }

        public Subscriber() {
        }

        public void PerformAction(Message item) {
            _subscriberAction(item);
        }
    }
}
