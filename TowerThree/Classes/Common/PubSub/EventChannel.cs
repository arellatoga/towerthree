﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerThree {
    public class EventChannel<Message> {
        public string _eventName;
        private List<KeyValuePair<string, Subscriber<Message>>> _subscribers;

        public EventChannel(string eventName) {
            _subscribers = new List<KeyValuePair<string, Subscriber<Message>>>();
            _eventName = eventName;
        }

        public bool AddSubscriber(Subscriber<Message> subscriber) {
            try {
                _subscribers.Add(new KeyValuePair<string, Subscriber<Message>>(subscriber._name, subscriber));
                return true;
            }
            catch {
                return false;
            }
        }

        public void SendMessage(Message item) {
            foreach(KeyValuePair<string, Subscriber<Message>> subscriber in _subscribers) {
                subscriber.Value.PerformAction(item);
            }
        }
    }
}
