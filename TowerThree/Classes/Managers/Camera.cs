﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerThree {
    class Camera : Publisher<float> {
        private Matrix _transform;
        private Vector2 _center;
        private Viewport _viewport;
        private float _zoom = 2.5f;
        private float _rotation = 0;
        private float _rotValue = 5f;

        public Publisher<Message> _publisher;

        public Matrix TRANSFORM {
            get => _transform;
        }

        public Vector2 CENTER {
            get => _center; set => _center = value;
        }

        public float ZOOM {
            get => _zoom; set => _zoom = value < 0.1f ? 0.1f : _zoom;
        } 

        public float ROTATION {
            get => _rotation; set => _rotation = value;
        }

        public Camera(Viewport viewport) {
            _viewport = viewport;
            _publisher = new Publisher<Message>();
        }

        public void Update(Vector2 position) {
            if (Keyboard.GetState().IsKeyDown(Keys.Q)) {
                _rotation += _rotValue;
                List<float> floats = new List<float>();
                floats.Add(-MathHelper.ToRadians(_rotValue));
                Message message = new Message("cameraRotation", "Camera", floats: floats);
                _publisher.Trigger("rotation", message);
            }
            else if (Keyboard.GetState().IsKeyDown(Keys.E)) {
                _rotation -= _rotValue;;
                List<float> floats = new List<float>();
                floats.Add(MathHelper.ToRadians(_rotValue));
                Message message = new Message("cameraRotation", "Camera", floats: floats);
                _publisher.Trigger("rotation", message);
            }
            _center = new Vector2(position.X, position.Y);
            _transform = Matrix.CreateTranslation(new Vector3(-_center.X, -_center.Y, 0)) *
                Matrix.CreateRotationZ(MathHelper.ToRadians(_rotation)) *
                Matrix.CreateScale(new Vector3(_zoom, _zoom, 0)) *
                Matrix.CreateTranslation(new Vector3(_viewport.Width / 2, _viewport.Height / 2, 0));
        }
    }
}
