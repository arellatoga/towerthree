﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerThree {
    class Enemy : Actor {      
        public Enemy(Texture2D texture, float x, float y, int rows, int cols) :
            base(texture, x, y, rows, cols) {
            _speed = 100f;

            _camRotSubscriber._name = "slime";
            _camRotSubscriber._subscriberAction = (rotation) => {
                _body.Rotation += rotation._floats[0];
            };

            InitAnimations();
        }

        private new void InitAnimations() {
            //_originOffset = new Vector2(_spriteWidth/2, _spriteHeight/2);

            _sprite._animList = new Dictionary<string, AnimData>();
            _sprite._animList.Add("idle", new AnimData("idle", 0, 0, 4, 1000 / 12));
            _sprite._animList.Add("run", new AnimData("run", 0, 4, 4, 1000 / 12));
            _sprite._animList.Add("attack", new AnimData("attack", 1, 0, 5, 1000 / 12));

            ChangeAnimState("attack", false);
        }

        public void Update(GameTime gameTime) {
            InputUpdate(gameTime);
            AnimationUpdate(gameTime);
        }
        private void InputUpdate(GameTime gameTime) {
        }

        public void Draw(SpriteBatch spriteBatch) {
            DrawAnimation(spriteBatch);
        }
    }
}
