﻿using FarseerPhysics.Collision;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerThree {
    public class Actor {
        // Meta
        public static string TYPE = "ACTOR";
        public Guid guid { get; private set; }

        // Position and physics
        protected float _speed; // speed makes velocity
        public Body _body;
        public Fixture _fixture;
        public Vector2 _positionOffset { get; protected set; }

        public Subscriber<Message> _camRotSubscriber;

        private List<Component> _components;

        protected Sprite _sprite;
       
        public Actor(Texture2D texture, float x, float y, int rows, int cols) {
            _camRotSubscriber = new Subscriber<Message>();
            guid = Guid.NewGuid();
            _sprite = new Sprite(texture, rows, cols);
            _components = new List<Component>();
        }

        public void setCircleBody(World world, Vector2 pos, BodyType bodyType, float friction = -1f) {
           _body = BodyFactory.CreateCircle(
                    world: world,
                    radius: _sprite._spriteLowerBound/2,
                    density: 0f,
                    position: pos
                );
            _body.BodyType = bodyType;
            if (friction >= 0f) {
                _body.Friction = friction;
            }
        }

        // Always override this
        protected void InitAnimations() {
        }

        // Component functions
        public void AddComponent(Component newComponent) {
            foreach (Component _component in _components) {
                if (newComponent._name == _component._name) {
                    Console.WriteLine("New component",
                        newComponent._name,
                        "already exists in", 
                        guid.ToString());
                    return;
                }
            }
            _components.Add(newComponent);
        }

        public Component FindComponent(string name) {
            foreach(Component _component in _components) {
                if (_component._name == name) {
                    return _component;
                }
            }
            return null;
        }

        // Switch to a new animation state as defined in InitAnimations()
        protected void ChangeAnimState(string anim, bool animLocked) {
            _sprite.ChangeAnimState(anim, animLocked);
        }

        // Swap to a new texture
        protected void ChangeTextures(string tex) {
            _sprite.ChangeTextures(tex);
        }

        // called every Update(). Handle animations
        protected void AnimationUpdate(GameTime gameTime) {
            _sprite.SpriteUpdate(gameTime);
        }

        public virtual void OnBroadphaseCollision(ref FixtureProxy fp1, ref FixtureProxy fp2) {
        }

        public virtual bool OnCollision(Fixture f1, Fixture f2, ContactManager contact) {
            return true;
        }
        
        public virtual bool BeginContact (Contact contact) {
            return true;
        }
        
        public virtual bool EndContact (Contact contact) {
            return true;
        }
        
        public virtual bool PresOlve (Contact contact, ref Manifold oldManifold) {
            return true;
        }
        
        public virtual bool PostSolve (Contact contact, ref Manifold oldManifold) {
            return true;
        }

        protected void DrawAnimation(SpriteBatch spriteBatch) {
            // TODO move sprite anim to sprite component
            int row = (int)((float)_sprite._currentFrame / _sprite._columns);
            int column = _sprite._currentFrame % _sprite._columns;

            Rectangle textureRegion = new Rectangle(_sprite._spriteWidth * column, 
                _sprite._spriteHeight * row, 
                _sprite._spriteWidth, 
                _sprite._spriteHeight);
            Rectangle positionRectangle = new Rectangle((int)(_body.Position.X), 
                (int)(_body.Position.Y), 
                _sprite._spriteWidth, 
                _sprite._spriteHeight);

            SpriteEffects flipSprite = SpriteEffects.None;
            if (_sprite.flipX) {
                flipSprite = SpriteEffects.FlipHorizontally;
            }

            spriteBatch.Draw(_sprite._texture,
                destinationRectangle: positionRectangle,
                sourceRectangle: textureRegion,
                //origin: _originOffset,
                color: Color.White,
                //rotation: _rotation,
                rotation: _body.Rotation,
                effects: flipSprite);
        }
    }
} 