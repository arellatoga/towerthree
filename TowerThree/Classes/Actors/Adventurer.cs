﻿using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerThree {
    class Adventurer : Actor {
        public Adventurer(Texture2D texture, float x, float y, int rows, int cols) :
            base(texture, x, y, rows, cols) {
            _speed = 100f;

            _camRotSubscriber._name = "adventurer";
            _camRotSubscriber._subscriberAction = (rotation) => {
                _body.Rotation += rotation._floats[0];
            };
            InitAnimations();
        }

        private new void InitAnimations() {
            //_originOffset = new Vector2(_spriteWidth / 2, _spriteHeight / 2);

            _sprite._animList = new Dictionary<string, AnimData>();
            _sprite._animList.Add("idle", new AnimData("idle", 0, 0, 4, 1000 / 12));
            _sprite._animList.Add("run", new AnimData("run", 1, 1, 6, 1000 / 12));
            _sprite._animList.Add("attack", new AnimData("attack", "idle", true, 6, 0, 8, 1000 / 12));

            ChangeAnimState("idle", false);
        }

        public void Update(GameTime gameTime) {
            InputUpdate(gameTime);
            AnimationUpdate(gameTime);
        }

        private void InputUpdate(GameTime gameTime) {
            KeyboardState state = Keyboard.GetState();
            int movX = 0;
            int movY = 0;

            if (_sprite._animLocked) {
                return;
            }

            if (state.IsKeyDown(Keys.J)) {
                _body.LinearVelocity = Vector2.Zero;
                ChangeAnimState("attack", true);
                return;
            }

            if (state.IsKeyDown(Keys.D)) {
                movX = 1;
                _sprite.flipX = false;
            }
            else if (state.IsKeyDown(Keys.A)) {
                movX = -1;
                _sprite.flipX = true;
            }
            if (state.IsKeyDown(Keys.S)) {
                movY = 1;
            }
            else if (state.IsKeyDown(Keys.W)) {
                movY = -1;
            }

            if (movX != 0 || movY != 0) {
                var velocity = _speed * 1f/60f * new Vector2(movX, movY);
                var rotated = Vector2.Transform(new Vector2(movX * 500, movY * 500), Matrix.CreateRotationZ(_body.Rotation));
                //_body.ApplyLinearImpulse(rotated);
                _body.LinearVelocity = rotated;
                ChangeAnimState("run", false);
            }
            else {
                _body.LinearVelocity = Vector2.Zero;
                ChangeAnimState("idle", false);
            }
        }

        override public void OnBroadphaseCollision(ref FixtureProxy fp1, ref FixtureProxy fp2) {
            //Console.WriteLine("Test");
        }

        public override bool OnCollision(Fixture f1, Fixture f2, ContactManager contact) {
            return base.OnCollision(f1, f2, contact);
        }

        public override bool BeginContact(Contact contact) {
            return true;
        }

        public void Draw(SpriteBatch spriteBatch) {
            DrawAnimation(spriteBatch);
        }
    }
}
