﻿using FarseerPhysics.Controllers;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace TowerThree
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        private Adventurer adventurer;
        private Enemy skeleton;
        private Enemy skeleton2;

        private Camera camera;

        private World _world;

        private delegate void DrawMethod(SpriteBatch sb);
        private SortedList<int, DrawMethod> _spriteDrawList;
        
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            LoadContentPhysics();
            LoadContentActors();
            LoadContentMeta();
        }

        private void LoadContentPhysics() {
            _world = new World(new Vector2(0, 0));
            spriteBatch = new SpriteBatch(GraphicsDevice);

            GravityController gravityController = new GravityController(0f);
            _world.AddController(gravityController);
        }

        private void LoadContentMeta() {
            camera = new Camera(GraphicsDevice.Viewport);
            EventChannel<Message> rotationPool = new EventChannel<Message>("rotation");
            rotationPool.AddSubscriber(adventurer._camRotSubscriber);
            rotationPool.AddSubscriber(skeleton._camRotSubscriber);
            rotationPool.AddSubscriber(skeleton2._camRotSubscriber);
            camera._publisher.AddEventChannel(rotationPool);

            _spriteDrawList = new SortedList<int, DrawMethod>();
            _spriteDrawList.Add(1, adventurer.Draw);
            _spriteDrawList.Add(0, skeleton.Draw);
            _spriteDrawList.Add(-1, skeleton2.Draw);
        }

        private void LoadContentActors() {
            Texture2D adventurerSheetTexture = Content.Load<Texture2D>("adventurer-sheet");
            Texture2D skeletonSheetTexture = Content.Load<Texture2D>("slime-sheet");

            adventurer = new Adventurer(adventurerSheetTexture, 0, 0, 11, 7);
            skeleton = new Enemy(skeletonSheetTexture, 50, 50, 3, 8);
            skeleton2 = new Enemy(skeletonSheetTexture, -50, -50, 3, 8);

            adventurer.setCircleBody(world: _world, pos: new Vector2(0, 0), bodyType: BodyType.Dynamic);
            skeleton.setCircleBody(world: _world, pos: new Vector2(50, 50), bodyType: BodyType.Static);
            skeleton2.setCircleBody(world: _world, pos: new Vector2(-50, 50), bodyType: BodyType.Static, friction: 0.5f);

            adventurer.AddComponent(new HealthComponent(10, 10));
            skeleton.AddComponent(new HealthComponent(5, 5));
            skeleton2.AddComponent(new HealthComponent(5, 5));

            adventurer._body.CollisionCategories = Category.Cat1;
            adventurer._body.CollidesWith = Category.All ^ Category.Cat2 ^ Category.Cat3;

            skeleton._body.CollisionCategories = Category.Cat2;
            skeleton2._body.CollisionCategories = Category.Cat3;
        }

        protected override void UnloadContent()
        {
            Content.Unload();
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
                Keyboard.GetState().IsKeyDown(Keys.Escape)) {
                Exit();
            }

            _world.Step(1f/160f);

            adventurer.Update(gameTime);
            skeleton.Update(gameTime);
            skeleton2.Update(gameTime);

            camera.Update(adventurer._body.Position);

            OnCollision();

            base.Update(gameTime);
        }

        private void OnCollision() {
            //_world.ContactManager.OnBroadphaseCollision += adventurer.OnBroadphaseCollision;
            //_world.ContactManager.BeginContact += adventurer.BeginContact;
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin(SpriteSortMode.Deferred,
                BlendState.AlphaBlend,
                null, null, null, null,
                camera.TRANSFORM);

            foreach(KeyValuePair<int, DrawMethod> kvp in _spriteDrawList) {
                kvp.Value(spriteBatch);
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
